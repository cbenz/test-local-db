export interface IListItem {
	id: string
	label: string
}

export interface IList {
	id: string
	label: string
	items: IListItem[]
}

export const findListById = (listId: string, lists: IList[]): IList | undefined =>
	lists.find((list) => list.id === listId)
