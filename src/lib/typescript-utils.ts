// Cf https://github.com/sveltejs/language-tools/issues/1575#issuecomment-1214279843
export function assertIsDefined<T>(a: T | undefined | null): T {
	return a! // eslint-disable-line @typescript-eslint/no-non-null-assertion
}
