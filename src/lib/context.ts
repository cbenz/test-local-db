import { getContext, setContext } from "svelte"

const appContextKey = {}

export interface IAppContext {
	appSection: string
}

export const getAppContext = () => getContext<IAppContext>(appContextKey)

export const setAppContext = (appContext: IAppContext) =>
	setContext<IAppContext>(appContextKey, appContext)
