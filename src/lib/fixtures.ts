import type { IList } from "./with-load/model"

export const sampleLists: IList[] = [
	{
		id: "food",
		label: "Food",
		items: [
			{ id: "chocolate", label: "Chocolate" },
			{ id: "banana", label: "Banana" },
			{ id: "mustard", label: "Mustard" },
		],
	},
	{
		id: "computers",
		label: "Computers",
		items: [
			{ id: "lenovo", label: "Lenovo" },
			{ id: "dell", label: "Dell" },
		],
	},
]
