import { deleteItemAtIndex, setItemAtIndex } from "$lib/array-utils"
import { createIndexedDBStorage, persist } from "@macfja/svelte-persistent-store"
import { writable, type Writable } from "svelte/store"
import { findListIndexById, withoutListItem, type IList } from "./model"

function _findListIndex(listId: string, lists: IList[]) {
	const listIndex = findListIndexById(listId, lists)
	if (listIndex === -1) {
		throw new Error(`List "${listId}" not found`)
	}
	return listIndex
}

const createListsStore = (store: Writable<IList[]>) => ({
	...store,
	deleteList(listId: string) {
		this.update((lists) => {
			const listIndex = _findListIndex(listId, lists)
			const newLists = deleteItemAtIndex(listIndex, lists)
			return newLists
		})
	},
	deleteItemFromList(listId: string, itemId: string) {
		this.update((lists) => {
			const listIndex = _findListIndex(listId, lists)
			const list = lists[listIndex]
			const newList = withoutListItem(itemId, list)
			const newLists = setItemAtIndex(listIndex, newList, lists)
			return newLists
		})
	},
})

export const lists = createListsStore(
	persist(writable<IList[]>([]), createIndexedDBStorage(), "lists:with-store"),
)
