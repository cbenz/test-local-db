import { deleteItemAtIndex } from "../array-utils"

export interface IListItem {
	id: string
	label: string
}

export interface IList {
	id: string
	label: string
	items: IListItem[]
}

export const withoutListItem = (itemId: string, list: IList) => {
	const { items } = list
	const itemIndex = items.findIndex((item) => item.id === itemId)
	if (itemIndex === -1) {
		throw new Error(`Item "${itemId}" not found in list "${list.id}"`)
	}
	const newItems = deleteItemAtIndex(itemIndex, items)
	return { ...list, items: newItems }
}

export const findListById = (listId: string, lists: IList[]): IList | undefined =>
	lists.find((list) => list.id === listId)

export const findListIndexById = (listId: string, lists: IList[]): number =>
	lists.findIndex((list) => list.id === listId)
