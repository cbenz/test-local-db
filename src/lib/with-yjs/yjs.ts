import { readable, type Readable } from "svelte/store"
import { IndexeddbPersistence } from "y-indexeddb"
import type { Awareness } from "y-protocols/awareness"
import { WebrtcProvider } from "y-webrtc"
import { WebsocketProvider } from "y-websocket"
import * as Y from "yjs"

export interface IYStore {
	ydoc: Y.Doc
	webrtcProvider?: WebrtcProvider
	websocketProvider?: WebsocketProvider
}

export function createYStore({ roomName }: { roomName: string }): Readable<IYStore> {
	const ydoc = new Y.Doc()
	return readable<IYStore>({ ydoc }, (set) => {
		console.log("YDocStore subscribe")
		const indexeddbPersistence = new IndexeddbPersistence(roomName, ydoc)
		const webrtcProvider = new WebrtcProvider(roomName, ydoc)
		const websocketProvider = new WebsocketProvider("ws://localhost:1234", roomName, ydoc)
		set({ ydoc, webrtcProvider, websocketProvider })
		return () => {
			console.log("YDocStore unsubscribe")
			indexeddbPersistence.destroy()
			webrtcProvider.destroy()
			websocketProvider.destroy()
		}
	})
}
export type ToYjsSharedTypes<T> = T extends Array<infer V>
	? Y.Array<ToYjsSharedTypes<V>>
	: T extends object
	? Y.Map<ToYjsSharedTypes<T[keyof T]>>
	: T

const toYjsSharedTypes = <T>(value: T): ToYjsSharedTypes<T> =>
	(Array.isArray(value)
		? Y.Array.from(value.map(toYjsSharedTypes))
		: typeof value === "object"
		? new Y.Map(Object.entries(value).map(([k, v]) => [k, toYjsSharedTypes(v)]))
		: value) as ToYjsSharedTypes<T>

export type NestedYjsSharedTypes<T> = T extends Array<infer V>
	? ToYjsSharedTypes<V>[]
	: T extends Record<infer K extends string, infer V>
	? Record<K, ToYjsSharedTypes<V>>
	: T

const wrapNestedAsYjsSharedTypes = <T>(value: T): NestedYjsSharedTypes<T> =>
	(Array.isArray(value)
		? value.map(toYjsSharedTypes)
		: typeof value === "object"
		? Object.fromEntries(Object.entries(value).map(([k, v]) => [k, toYjsSharedTypes(v)]))
		: value) as NestedYjsSharedTypes<T>

export type YReadableArray<T> = Readable<T[]> & {
	wrapNested: typeof wrapNestedAsYjsSharedTypes
	yvalue: Y.Array<ToYjsSharedTypes<T>>
}

export function createYArrayStore<T>(ydoc: Y.Doc, name: string): YReadableArray<T> {
	const yarray = ydoc.getArray<ToYjsSharedTypes<T>>(name)
	const store = readable<T[]>(yarray.toJSON(), (set) => {
		yarray.observeDeep((event, tr) => {
			console.log(event, tr)
			set(yarray.toJSON())
		})
	})
	return {
		...store,
		wrapNested: wrapNestedAsYjsSharedTypes,
		yvalue: yarray,
	}
}

export type YReadableAwareness<T> = Readable<T[]> & {
	setLocalStateField: Awareness["setLocalStateField"]
}

export function createAwarenessStore<T>(awareness: Awareness): YReadableAwareness<T> {
	const store = readable<T[]>(undefined, (set) => {
		function handleChange() {
			const newValue = Array.from(awareness.getStates().values()) as T[]
			set(newValue)
		}
		awareness.on("change", handleChange)
		return () => {
			awareness.off("change", handleChange)
			// awareness.destroy()
		}
	})
	return { ...store, setLocalStateField: awareness.setLocalStateField.bind(awareness) }
}

export type WebsocketConnectionStatus = "disconnected" | "connecting" | "connected"

export function createWebsocketStores(websocketProvider: WebsocketProvider) {
	const status = readable<WebsocketConnectionStatus>("disconnected", (set) => {
		function handleStatus({ status }: { status: WebsocketConnectionStatus }) {
			console.log("status", status)
			set(status)
		}
		websocketProvider.on("status", handleStatus)
		return () => {
			websocketProvider.off("status", handleStatus)
		}
	})
	const sync = readable<boolean>(false, (set) => {
		function handleSync(isSynced: boolean) {
			console.log("sync", isSynced)
			set(isSynced)
		}
		websocketProvider.on("sync", handleSync)
		return () => {
			websocketProvider.off("sync", handleSync)
		}
	})
	return { status, sync }
}
