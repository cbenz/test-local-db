import { get } from "svelte/store"
import type * as Y from "yjs"
import type { ToYjsSharedTypes, YReadableArray } from "./yjs"

export interface IAwarenessData {
	user: IAwarenessUserData
}

export interface IAwarenessUserData {
	color: string
	nickname: string
}

export interface IListItem {
	id: string
	label: string
}

export interface IList {
	id: string
	label: string
	items: IListItem[]
}

export const deleteItemFromList = (item: IListItem, list: IList, lists: YReadableArray<IList>) => {
	const $lists = get(lists)
	const listIndex = $lists.indexOf(list)
	const itemIndex = list.items.indexOf(item)
	if (listIndex === -1) {
		throw new Error(`Could not find list "${list.id}"`)
	}
	if (itemIndex === -1) {
		throw new Error(`Could not find list item "${item.id}" in list "${list.id}"`)
	}
	const ylist = lists.yvalue.get(listIndex)
	const yitems = ylist.get("items") as Y.Array<ToYjsSharedTypes<IListItem>>
	yitems.delete(itemIndex)
}

export const deleteList = (list: IList, lists: YReadableArray<IList>) => {
	const $lists = get(lists)
	const index = $lists.indexOf(list)
	lists.yvalue.delete(index, 1)
}

export const findListById = (listId: string, lists: IList[]): IList | undefined =>
	lists.find((list) => list.id === listId)
