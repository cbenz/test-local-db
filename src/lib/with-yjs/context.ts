import { getContext, setContext } from "svelte"
import type { Readable } from "svelte/store"
import type { IList } from "./model"
import type { IYStore, YReadableArray } from "./yjs"

const yjsContextKey = {}

export interface IYjsContext {
	getLists: () => YReadableArray<IList>
	getYStore: () => Readable<IYStore>
}

export const getYjsContext = () => getContext<IYjsContext>(yjsContextKey)

export const setYjsContext = (yjsContext: IYjsContext) =>
	setContext<IYjsContext>(yjsContextKey, yjsContext)
