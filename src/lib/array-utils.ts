export const deleteItemAtIndex = <T>(index: number, items: T[]): T[] => [
	...items.slice(0, index),
	...items.slice(index + 1),
]

export const setItemAtIndex = <T>(index: number, newItem: T, items: T[]): T[] => [
	...items.slice(0, index),
	newItem,
	...items.slice(index + 1),
]
