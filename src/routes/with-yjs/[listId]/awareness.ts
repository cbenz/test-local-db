import type { IAwarenessUserData } from "$lib/with-yjs/model"
import { generate as generateUserName } from "do_username"

const colors = [
	"#30bced",
	"#6eeb83",
	"#ffbc42",
	"#ecd444",
	"#ee6352",
	"#9ac2c9",
	"#8acb88",
	"#1be7ff",
]

export const generateColor = (): string => colors[Math.floor(Math.random() * colors.length)]

export const generateNickname = (): string => generateUserName(15)

export const generateUserData = (): IAwarenessUserData => ({
	color: generateColor(),
	nickname: generateNickname(),
})
