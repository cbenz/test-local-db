import { findListById } from "$lib/with-load/model"
import { error } from "@sveltejs/kit"
import type { PageLoad } from "./$types"

export const load: PageLoad = async ({ params, parent }) => {
	const { listId } = params
	const { lists } = await parent()
	const list = findListById(listId, lists)
	if (!list) {
		throw error(404, `The "${listId}" list was not found.`)
	}
	return { list }
}
