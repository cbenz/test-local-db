import { sampleLists } from "$lib/fixtures"
import type { LayoutLoad } from "./$types"

export const load: LayoutLoad = () => ({ appSection: "with-load", lists: sampleLists })
