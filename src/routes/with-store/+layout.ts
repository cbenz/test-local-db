import type { LayoutLoad } from "./$types"

export const load: LayoutLoad = () => ({ appSection: "with-store" })
